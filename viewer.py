import zmq
import logging
from logging.handlers import SocketHandler

def begin():

    log = logging.getLogger('Spyglass')
    log.setLevel(1)
    socket_handler = SocketHandler('127.0.0.1', 19996)
    log.addHandler(socket_handler)

    log.info("Starting Spyglass Logging")

    # Log File Messages
    # zmq port 13270
    evelog_context = zmq.Context()
    evelog_socket = evelog_context.socket(zmq.SUB)

    print(
    "Collecting updates from zmq server...")
    evelog_socket.connect("tcp://127.0.0.1:13270")
    evelog_socket.setsockopt_string(zmq.SUBSCRIBE, '')

    evelog_log = log.getChild('evelog')


    while True:
        # evelog
        string = evelog_socket.recv_string()
        print(string)
        evelog_log.info(string)


if __name__ == "__main__":
    begin()