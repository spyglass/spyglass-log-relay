# Spyglass Log Viewer

A real time insight into the internal communications within Spyglass-NG. 

## Getting Started

Simply run the script on the same computer that is running Spyglass-NG, and then open cutelog. The default configuration can be found below:

cutelog 127.0.0.1:19996

## Spyglass-NG Listener

Listens on the following localhost tcp ports by default

| log | port |
|-----|------|
| evelog | 13270 |

